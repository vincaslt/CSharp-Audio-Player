﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using AudioPlayer.Domain;

namespace AudioPlayer.Forms
{
    public partial class FormMain : Form
    {
        private Thread _playingThread;
        //TODO handle exceptions

        delegate void SetValueCallback(int value);
        delegate void SetLengthCallback(TimeSpan current, TimeSpan total);

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            SizeLastColumn();
        }

        private void SizeLastColumn()
        {
            playlist.Columns[playlist.Columns.Count - 1].Width = -2;
        }

        private void FormMain_Resize(object sender, EventArgs e)
        {
            SizeLastColumn();
        }

        private void playlist_ItemActivate(object sender, EventArgs e)
        {
            Play(true);
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            if (openPlaylistDialog.ShowDialog() == DialogResult.OK)
            {
                Player.Playlist.Import(openPlaylistDialog.FileName);
            }
            RefreshPlaylist();
        }

        private void RefreshPlaylist(string filter = "")
        {
            textboxSearch.Text = filter;
            playlist.Items.Clear();
            
            var i = 0;
            foreach (var song in Player.Playlist.Songs)
            {
                if (!song.Title.ToLower().Contains(filter) &&
                    !song.Artist.ToLower().Contains(filter)) continue;

                var item = new ListViewItem((++i).ToString());
                item.SubItems.Add(song.Title);
                item.SubItems.Add(song.Length.ToString("g"));
                item.SubItems.Add(song.Artist);
                item.SubItems.Add(song.Album);
                item.SubItems.Add(song.Genre);
                item.Tag = song;

                playlist.Items.Add(item);
            }
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Player.Playlist.Export(saveFileDialog.FileName);
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (importAudioDialog.ShowDialog() == DialogResult.OK)
            {
                foreach (var file in importAudioDialog.FileNames)
                {
                    Player.Playlist.Add(Song.FromFile(file));
                }
            }
            RefreshPlaylist();
        }

        private void buttonPlay_Click(object sender, EventArgs e)
        {
            Play();
        }

        private void Play(bool forced = false)
        {
            //TODO proper image swapping

            var song = Player.Playing;
            if (Player.PlayerStatus == Player.Status.Playing && !forced)
            {
                buttonPlay.BackgroundImage = Properties.Resources.play;
                song.Pause();
            }
            else
            {
                if (playlist.SelectedItems.Count > 0)
                {
                    song = (Song)playlist.SelectedItems[0].Tag;
                }

                if (song == null) return;

                trackbar.Maximum = (int)song.Length.TotalSeconds;
                buttonPlay.BackgroundImage = Properties.Resources.pause;
                KillTrackingThread();
                song.Play();
                _playingThread = new Thread(TrackSong);
                _playingThread.Start();
                labelSongName.Text = song.Artist + " - " + song.Title;
            }
        }

        private void TrackSong()
        {
            while (Player.PlayerStatus == Player.Status.Playing)
            {
                if (Player.Playing != null)
                {
                    var timePlayed = TimeSpan.FromMilliseconds(Player.Playing.Length.TotalMilliseconds - 
                                                                    Player.Playing.MilliSecondsRemaining);
                    SetTrackbar((int)timePlayed.TotalSeconds);
                    SetLengthText(timePlayed, Player.Playing.Length);
                }
            }
        }

        private void SetTrackbar(int value)
        {
            try
            {
                if (trackbar.InvokeRequired)
                {
                    SetValueCallback callback = SetTrackbar;
                    Invoke(callback, value);
                }
                else
                {
                    trackbar.Value = value;
                }
            }
            catch (Exception)
            {
                // Do nothing?
            }
        }

        private void SetLengthText(TimeSpan current, TimeSpan total)
        {
            try
            {
                if (trackbar.InvokeRequired)
                {
                    SetLengthCallback callback = SetLengthText;
                    Invoke(callback, current, total);
                }
                else
                {
                    labelLength.Text =
                        current.Hours + ":" + current.Minutes + ":" + current.Seconds + "/" +
                        total.Hours + ":" + total.Minutes + ":" + total.Seconds;
                }
            }
            catch (Exception)
            {
                // Do nothing?
            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            //TODO
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (Player.Playing != null)
            {
                Player.Playing.Stop();
            }
        }

        private void playlist_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column != 1) return;

            Player.Playlist.Songs.Sort();
            RefreshPlaylist();
        }

        private void textboxSearch_Changed(object sender, EventArgs e)
        {
            RefreshPlaylist(textboxSearch.Text);
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (Player.PlayerStatus == Player.Status.Playing)
            {
                KillTrackingThread();
                Player.Playing.Stop();       
                buttonPlay.BackgroundImage = Properties.Resources.play;
                trackbar.Value = 0;
                labelLength.Text = "Stopped";
            }
        }

        private void KillTrackingThread()
        {
            if (_playingThread != null)
            {
                _playingThread.Abort();
            }
        }
    }
}