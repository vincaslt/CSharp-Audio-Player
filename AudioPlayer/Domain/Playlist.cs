﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AudioPlayer.Domain
{
    internal class Playlist : IEnumerable<string>
    {
        public Playlist()
        {
            Albums = new List<string>();
            Artists = new List<string>();
            Genres = new List<string>();
            Songs = new List<Song>();
        }

        public List<string> Albums { get; private set; }
        public List<string> Artists { get; private set; }
        public List<string> Genres { get; private set; }
        public List<Song> Songs { get; private set; }

        public void Add(Song song)
        {
            Songs.Add(song);

            if (!Genres.Contains(song.Genre))
            {
                Genres.Add(song.Genre);
            }

            if (!Albums.Contains(song.Album))
            {
                Albums.Add(song.Album);
            }

            if (!Artists.Contains(song.Artist))
            {
                Artists.Add(song.Artist);
            }
        }

        public IEnumerator<string> GetEnumerator()
        {
            return Songs.Select(song => song.File).ToList().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Import(string file)
        {
            var files = File.ReadAllLines(file);
            foreach (var songfile in files)
            {
                Add(Song.FromFile(songfile));
            }
        }

        public void Export(string file)
        {
            File.WriteAllLines(file, this);
        }
    }
}