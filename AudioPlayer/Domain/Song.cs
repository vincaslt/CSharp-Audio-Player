﻿using System;
using System.Threading;
using CSCore.Codecs;
using CSCore.SoundOut;

namespace AudioPlayer.Domain
{
    internal class Song : IComparable<Song>, IEquatable<Song>
    {
        private Thread _thread;
        private ISoundOut _soundOut;

        public Song(string album, string artist, string file, string genre, TimeSpan length, string title)
        {
            Album = album;
            Artist = artist;
            File = file;
            Genre = genre;
            Length = length;
            Title = title;
        }

        public string Album { get; private set; }
        public string Artist { get; private set; }
        public string File { get; private set; }
        public string Genre { get; private set; }
        public TimeSpan Length { get; private set; }
        public double MilliSecondsRemaining { get; private set; }
        public string Title { get; private set; }

        public static Song FromFile(string file)
        {
            var tagFile = TagLib.File.Create(file);
            return new Song
                (
                tagFile.Tag.Album,
                tagFile.Tag.FirstAlbumArtist ?? tagFile.Tag.FirstPerformer,
                file,
                tagFile.Tag.FirstGenre,
                tagFile.Properties.Duration,
                tagFile.Tag.Title
                );
        }

        public void Play()
        {
            if (Player.Playing != null && Player.Playing != this)
            {
                Player.Playing.Stop();
            }

            switch (Player.PlayerStatus)
            {
                case Player.Status.Playing:
                case Player.Status.Stopped:
                    _thread = new Thread(PlaySong);
                    _thread.Start();
                    break;
                case Player.Status.Paused:
                    _soundOut.Resume();
                    break;
            }
            Player.PlayerStatus = Player.Status.Playing;
        }

        public void Stop()
        {
            Player.PlayerStatus = Player.Status.Stopped;
            //MilliSecondsRemaining = 0;

            if (_soundOut != null)
            {
                _soundOut.Stop();
                _soundOut.Dispose();
                _soundOut = null;
            }
            
        }

        public void Pause()
        {
            Player.PlayerStatus = Player.Status.Paused;
            _soundOut.Pause();
        }

        private void PlaySong()
        {
            _soundOut = GetSoundOut();
            _soundOut.Initialize(CodecFactory.Instance.GetCodec(@File));
            Player.Playing = this;
            _soundOut.Play();

            MilliSecondsRemaining = Length.TotalMilliseconds;
            while (Player.PlayerStatus != Player.Status.Stopped && MilliSecondsRemaining > 0)
            {
                if (Player.PlayerStatus == Player.Status.Playing)
                {
                    Thread.Sleep(1);
                    MilliSecondsRemaining--;
                }
            }

            if (MilliSecondsRemaining <= 0)
            {
                Stop();
            }
        }

        private static ISoundOut GetSoundOut()
        {
            if (WasapiOut.IsSupportedOnCurrentPlatform)
            {
                return new WasapiOut();
            }

            return new DirectSoundOut();
        }

        public int CompareTo(Song other)
        {
            return string.Compare(Title, other.Title, StringComparison.Ordinal);
        }

        public bool Equals(Song other)
        {
            return Title.Equals(other.Title) && Length.Equals(other.Length) && Artist.Equals(other.Artist);
        }
    }
}