﻿using System.IO;

namespace AudioPlayer.Domain
{
    internal class Player
    {
        public enum Status
        {
            Playing, Paused, Stopped
        }

        private static Status _status = Status.Stopped;
        public static Status PlayerStatus { get { return _status; } set { _status = value; } }

        private static Playlist _playlist = new Playlist();

        public static Playlist Playlist
        {
            get { return _playlist; }
            set { _playlist = value; }
        }

        public static Song Playing { get; set; }
       
    }
}